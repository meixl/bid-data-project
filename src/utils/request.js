import axios from 'axios';

axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8';

// 创建axios实例
const service = axios.create({
	// axios中请求配置有baseURL选项，表示请求URL公共部分
	baseURL: process.env.VUE_APP_BASE_API,
	// 超时
	timeout: 30000
});

// request拦截器
service.interceptors.request.use(config => {
	return config;
}, error => {
	errorDeal(error);
	Promise.reject(error);
});

// 响应拦截器
service.interceptors.response.use(res => {
	const code = Number(res.data.code) || 200;
	const message = res.data.msg;
	const url = res.config.url;
	if (code === 200) {
		return res.data;
	} else {
		errorDeal({
			url,
			response: {
				status: code,
				message: message
			}
		});
		return Promise.reject(res.data);
	}
},
error => {
	errorDeal(error);
	return Promise.reject(error);
}
);

// 不抛异常的白名单
const noShowErr = [];

function errorDeal (error) {
	console.log(error);
	if (error.url && error.url.indexOf(noShowErr.join(';;')) !== -1) {
		return;
	}
}

export default service;
