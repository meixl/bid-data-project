import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'index',
		component: () => import('@/views/index'),
		redirect: '/lu'
	},
	{
		path: '/lu',
		name: 'lu',
		component: () => import('@/views/LU-DA-PING/index'),
		meta: { title: 'Cs大屏' }
	},
	{
		path: '/command-center',
		name: 'command-center',
		component: () => import('@/views/NH-WX-command-center/index'),
		meta: { title: 'NH网信指挥中心' }
	},
];

const router = new VueRouter({
	routes
});

export default router;
