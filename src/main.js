import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// 将自动注册所有组件为全局组件
import dataV from '@jiaminghi/data-view';
// 按需引入仅支持基于ES module的tree shaking
import { borderBox1 } from '@jiaminghi/data-view';
// 引入自适应
import 'lib-flexible/flexible';
import * as echarts from 'echarts';
import 'echarts-gl';

// 引入mockjs
import './mock';

// 挂载到vue的原型
Vue.prototype.$echarts = echarts;
Vue.use(borderBox1);
Vue.use(dataV);
Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');
