const Mock = require('mockjs');

/*
	获取市男女比例
*/
const getCityMaleFemaleRatio = (params) => {
	const result = {
		code: 200,
		data: [
			{
				value: 75,
				name: '男',
				itemStyle: {
					color: '#0efcf0',
					shadowColor: '#0efcf0',
					shadowBlur: 20,
				}
			},
			{
				value: 25,
				name: '女',
				itemStyle: {
					color: 'rgba(236, 145, 133, 0.88)',
					shadowColor: 'rgba(236, 145, 133, 0.88)',
					shadowBlur: 20,
				}
			}
		]
	};
	return result;
};

/*
  获取舆情新闻列表
*/
const getPublicOpinionList = (params) => {

};

Mock.mock('/maleFemaleRatio', 'get', getCityMaleFemaleRatio);
Mock.mock('/publicOpinion', 'get', getPublicOpinionList);
