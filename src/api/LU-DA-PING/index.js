import request from '@/utils/request';

const API = {
	getPublicOpinionList: '/publicOpinion'
};

/*
  获取舆情新闻列表
*/
export function getPublicOpinionList (params) {
	return request({
		url: API.getPublicOpinionList,
		method: 'GET',
		params
	});
}
