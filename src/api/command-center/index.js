import request from '@/utils/request';

const API = {
	getCityMaleFemaleRatio: '/maleFemaleRatio',
	getPublicOpinionList: '/publicOpinion'
};

/*
  获取舆情新闻列表
*/
export function getPublicOpinionList (params) {
	return request({
		url: API.getPublicOpinionList,
		method: 'GET',
		params
	});
}

/*
	获取市男女比例
*/
export function getCityMaleFemaleRatio (params) {
	return request({
		url: API.getCityMaleFemaleRatio,
		method: 'GET',
		params
	});
}
