const path = require('path');
// cnpm run build --report检测包大小
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionWebpackPlugin = require('compression-webpack-plugin');// 启用gzip
// 默认设置压缩图片
// const defaultOptions = {
// 	bypassOnDebug: true
// };
//  自定义设置压缩图片
const customOptions = {
	mozjpeg: {
		progressive: true,
		quality: 50
	},
	optipng: {
		enabled: true,
	},
	pngquant: {
		quality: [0.5, 0.65],
		speed: 4
	},
	gifsicle: {
		interlaced: false,
	},
	// 不支持WEBP就不要写这一项
	webp: {
		quality: 75
	}
};
function resolve (dir) {
	return path.join(__dirname, dir);
}
const vueConfig = {
	productionSourceMap: true, // 加速生产环境构建 打包时候不会出现.map文件
	// 简单配置
	configureWebpack: config => {
		if (process.env.NODE_ENV === 'production') {
			return {
				plugins: [
					new BundleAnalyzerPlugin(), // 检测包大小
					new CompressionWebpackPlugin()// 启用gzip配合nginx
				]
			};
		}
	},
	// 精细配置
	chainWebpack: config => {
		// 压缩图片
		config.module.rule('images')
			.test(/\.(gif|png|jpe?g|svg)$/i)
			.use('image-webpack-loader')
			.loader('image-webpack-loader')
			.options(customOptions)
			.end();
		config.resolve.alias
			.set('@', resolve('src'));
	}
};
module.exports = vueConfig;
