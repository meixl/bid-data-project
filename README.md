# datav-bigdata

### 使用之前

```bash
# 下载依赖
npm run install
# 运行
npm run serve
```

###### flexible插件修改说明

这是`flexible + rem`适配解决方案需要处理的步骤之一，需要修改`flexible`依赖文件，路径：`node_modules/lib-flexible/flexible.js`

```js
// 修改内容
function refreshRem(){
  var width = docEl.getBoundingClientRect().width;
  if (width / dpr < 1920) {
      width = 1920 * dpr;
  } else if (width / dpr > 5760) {
      width = 5760 * dpr;
  }
  var rem = width / 24;
  docEl.style.fontSize = rem + 'px';
  flexible.rem = win.rem = rem;
}
```

### 辅助开发说明

###### 组件库

- [DataV - Vue 大屏数据展示组件库](http://datav.jiaminghi.com/)
